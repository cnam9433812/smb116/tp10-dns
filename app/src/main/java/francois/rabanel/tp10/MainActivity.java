package francois.rabanel.tp10;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.net.ServerSocket;

import francois.rabanel.tp10.DNS.Client;
import francois.rabanel.tp10.DNS.Server;

public class MainActivity extends AppCompatActivity {
    String TAG = "MainActivity";
    Activity activity;
    Client myClient;
    Server myServer;
    ServerSocket serverSocket;
    TextView portTxt, socketStateTxt, serviceStateTxt, serviceNameTxt;
    NsdServiceInfo serviceRegistered;
    NsdManager nsdManager;
    private String SERVICE_NAME = "Deptinfo";
    private String SERVICE_TYPE = "_http._tcp";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.activity = this;

        // Init view
        Button startServiceBtn = findViewById(R.id.start_service_btn);
        Button stopServiceBtn = findViewById(R.id.stop_service_btn);
        portTxt = findViewById(R.id.port_txt);
        serviceNameTxt = findViewById(R.id.service_name_txt);
        serviceStateTxt = findViewById(R.id.service_state_txt);
        socketStateTxt = findViewById(R.id.socket_state_txt);


        Button startDiscoveryBtn = findViewById(R.id.start_discovery_btn);
        Button stopDiscoveryBtn = findViewById(R.id.stop_discovery_btn);
        Button sendMessageBtn = findViewById(R.id.send_message_btn);

        Button closeBtn = findViewById(R.id.close_btn);


        // Start service
        startServiceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myServer = new Server(SERVICE_NAME, SERVICE_TYPE);
                serverSocket = myServer.initializeServerSocket();
                Log.d(TAG, "serverSocket: " + serverSocket);

                // Starting thread with a socket accepting connections.
                myServer.start();

                serviceRegistered = myServer.registerService(
                        getApplicationContext(), serverSocket.getLocalPort()
                );
                Log.d(TAG, "serviceRegistered: " + serviceRegistered);

                updateServiceInformationView(
                        serverSocket, serviceRegistered, "Registered"
                );

                startServiceBtn.setEnabled(false);
                stopServiceBtn.setEnabled(true);
            }
        });

        // Stop service
        stopServiceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myServer.closeSocket(serverSocket);
                myServer.unregisterService();

                updateServiceInformationView(
                        serverSocket, serviceRegistered, "UnRegistered"
                );
                // Stop thread.
                myServer.interrupt();

                stopServiceBtn.setEnabled(false);
                startServiceBtn.setEnabled(true);
            }
        });

        // Start discovery
        startDiscoveryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myClient = new Client(activity);
                myClient.startDiscoverServices(getApplicationContext(), SERVICE_TYPE);

                startDiscoveryBtn.setEnabled(false);
                stopDiscoveryBtn.setEnabled(true);
            }
        });

        // Stop discovery
        stopDiscoveryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myClient.stopDiscoveringServices();

                stopDiscoveryBtn.setEnabled(false);
                startDiscoveryBtn.setEnabled(true);
            }
        });

        // Send message
        sendMessageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "MESSAGE SENT");
            }
        });

        // Close app
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myServer.unregisterService();
                myServer.closeSocket(serverSocket);
                myServer.interrupt();
                finish();
            }
        });

    }

    public String getSocketServiceState(ServerSocket serverSocket){
        String result = "";
        if (serverSocket.isBound()){
            result = "running";
        }

        if (serverSocket.isClosed()){
            result = "stopped";
        }

        return result;
    }
    public void updateServiceInformationView(
            ServerSocket serverSocket, NsdServiceInfo serviceRegistered, String serviceState
    ) {
        portTxt.setText("NSD Service started, port: " + serverSocket.getLocalPort());
        socketStateTxt.setText("Socket status: " + getSocketServiceState(serverSocket));
        serviceNameTxt.setText("Service name: " + serviceRegistered.getServiceName());
        serviceStateTxt.setText("Service state: " + serviceState);
    }


}