package francois.rabanel.tp10.DNS;

import android.app.Activity;
import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import francois.rabanel.tp10.R;

public class Client extends AppCompatActivity {
    private String TAG = "ClientClass";
    private InetAddress host;
    private Integer port;
    private NsdManager nsdManager;
    private NsdManager.DiscoveryListener discoveryListener;
    private NsdManager.ResolveListener resolveListener;
    private Activity activity;
    TextView discoveryStateTxt, discoveryServiceFoundedTxt, connectedOnTxt, connectedOnWithDetailsTxt;
    Button sendMessageBtn;

    public Client(Activity activity){
        this.activity = activity;
    }
    public void initializeDiscoveryListener() {
        discoveryStateTxt = (TextView) activity.findViewById(R.id.discovery_state_txt);
        discoveryServiceFoundedTxt = (TextView) activity.findViewById(R.id.discovery_service_found_txt);
        connectedOnTxt = (TextView) activity.findViewById(R.id.connected_on_service_txt);
        connectedOnWithDetailsTxt = (TextView) activity.findViewById(R.id.connected_on_service_with_details_txt);

        sendMessageBtn = (Button) activity.findViewById(R.id.send_message_btn);

        // Instantiate a new DiscoveryListener
        discoveryListener = new NsdManager.DiscoveryListener() {
            // Called as soon as service discovery begins.
            @Override
            public void onDiscoveryStarted(String regType) {
                Log.d(TAG, "Service discovery started");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        discoveryStateTxt.setText(
                                "Discovering started..."
                        );
                    }
                });
            }

            @Override
            public void onServiceFound(NsdServiceInfo service) {
                // A service was found! Do something with it.
                Log.d(TAG, "Service discovery success -> " + service);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        discoveryStateTxt.setText("Discovering started with success.");
                        discoveryServiceFoundedTxt.setText(
                                "Service founded: "
                                + "name: "
                                + service.getServiceName()
                                + ", type: "
                                + service.getServiceType()
                        );
                    }
                });

                // Connecting to the founded service.
                Log.d(TAG, "Connecting to serviceName -> " + service.getServiceName());
                initializeResolveListener(service.getServiceName());
                nsdManager.resolveService(service, resolveListener);
            }

            @Override
            public void onServiceLost(NsdServiceInfo service) {
                // When the network service is no longer available.
                // Internal bookkeeping code goes here.
                Log.e(TAG, "service lost: " + service);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        discoveryStateTxt.setText("Discovering service lost.");
                        discoveryServiceFoundedTxt.setText(
                                "Service losted: "
                                        + "name: "
                                        + service.getServiceName()
                                        + ", type: "
                                        + service.getServiceType()
                        );
                        connectedOnTxt.setText("");
                        connectedOnWithDetailsTxt.setText("");
                    }
                });
            }

            @Override
            public void onDiscoveryStopped(String serviceType) {
                Log.i(TAG, "Discovery stopped: " + serviceType);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        discoveryStateTxt.setText("Discovering stopped for " + serviceType);
                        connectedOnTxt.setText("Disconnected from " + serviceType);
                        connectedOnWithDetailsTxt.setText("");
                    }
                });
            }

            @Override
            public void onStartDiscoveryFailed(String serviceType, int errorCode) {
                Log.e(TAG, "Discovery failed: Error code:" + errorCode);
                nsdManager.stopServiceDiscovery(this);
            }

            @Override
            public void onStopDiscoveryFailed(String serviceType, int errorCode) {
                Log.e(TAG, "Discovery failed: Error code:" + errorCode);
                nsdManager.stopServiceDiscovery(this);
            }
        };
    }

    public void initializeResolveListener(String serviceName) {
        resolveListener = new NsdManager.ResolveListener() {

            @Override
            public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
                // Called when the resolve fails. Use the error code to debug.
                Log.e(TAG, "Resolve failed: " + errorCode);
            }

            @Override
            public void onServiceResolved(NsdServiceInfo serviceInfo) {
                Log.d(TAG, "Resolve Succeeded -> " + serviceInfo);

                if (serviceInfo.getServiceName().equals(serviceName)) {
                    host = serviceInfo.getHost();
                    port = serviceInfo.getPort();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            connectedOnTxt.setText("Connected to: " + serviceName);
                            connectedOnWithDetailsTxt.setText(
                                    "Port: " + port + ", Host: " + host
                            );
                            sendMessageBtn.setEnabled(true);
                        }
                    });
                    return;
                }
            }
        };
    }

    public void startDiscoverServices(Context contextApp, String serviceTypeToDiscover){
        Log.d(TAG, "Let's go discovering services...");
        initializeDiscoveryListener();

        nsdManager = (NsdManager) contextApp.getSystemService(Context.NSD_SERVICE);
        nsdManager.discoverServices(
                serviceTypeToDiscover, NsdManager.PROTOCOL_DNS_SD, discoveryListener);
    }

    public void stopDiscoveringServices(){
        Log.d(TAG, "Stopping discovering services...");
        nsdManager.stopServiceDiscovery(discoveryListener);
    }

    public void sendMessage() throws IOException {
        // Connect to socket
        // Send message
    }
}
