package francois.rabanel.tp10.DNS;

import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends  Thread {

    private String TAG = "ServerClass";
    private boolean done = false;
    private String serviceName, serviceType;
    private NsdManager.RegistrationListener registrationListener;
    private NsdManager nsdManager;
    private ServerSocket serverSocket = null;
    private Socket socket = null;

    public Server(String serviceName, String serviceType) {
        this.serviceName = serviceName;
        this.serviceType = serviceType;
    }

    public NsdServiceInfo registerService(Context contextApp, int port) {
        // Create the NsdServiceInfo object, and populate it.
        NsdServiceInfo serviceInfoObject = new NsdServiceInfo();

        serviceInfoObject.setServiceName(this.serviceName);
        serviceInfoObject.setServiceType(this.serviceType);
        serviceInfoObject.setPort(port);
        Log.d(TAG, "serviceInfoObject: " + serviceInfoObject);

        // Register the service.
        initializeRegistrationListener();
        this.nsdManager = (NsdManager) contextApp.getSystemService(Context.NSD_SERVICE);
        nsdManager.registerService(
                serviceInfoObject, NsdManager.PROTOCOL_DNS_SD, registrationListener
        );
        Log.d(TAG, "nsdManager: " + nsdManager);

        return serviceInfoObject;
    }

    public ServerSocket initializeServerSocket() {
        // Initialize a server socket on the next available port.
        try {
            serverSocket = new ServerSocket(0);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return serverSocket;
    }

    @Override
    public void run() {
        try {
            Log.i(TAG, "Thread started !");
            socket = serverSocket.accept();
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            while(true) {
                try {
                    System.out.println("Socket's reading...");
                    String line = in.readLine();
                    out.println(line);
                } catch (IOException e) {
                    System.out.println("Read failed");
                    System.exit(-1);
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "Thread interrupted !");
            e.printStackTrace();
        }
    }

    public void closeSocket(ServerSocket socket){
        try {
            socket.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public void unregisterService(){
        nsdManager.unregisterService(registrationListener);
    }
    public void initializeRegistrationListener() {
        registrationListener = new NsdManager.RegistrationListener() {

            @Override
            public void onServiceRegistered(NsdServiceInfo NsdServiceInfo) {
                Log.d(TAG, "Registration succeed.");

                // Save the service name. Android may have changed it in order to
                // resolve a conflict, so update the name you initially requested
                // with the name Android actually used.
                serviceName = NsdServiceInfo.getServiceName();
                Log.d(TAG, "Service registered: " + serviceName);


            }

            @Override
            public void onRegistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
                // Registration failed! Put debugging code here to determine why.
                Log.d(TAG, "Registration failed.");
            }

            @Override
            public void onServiceUnregistered(NsdServiceInfo arg0) {
                // Service has been unregistered. This only happens when you call
                // NsdManager.unregisterService() and pass in this listener.
                Log.d(TAG, "Service unregistered.");
            }

            @Override
            public void onUnregistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
                // Unregistration failed. Put debugging code here to determine why.
                Log.d(TAG, "Unregistration failed.");
            }
        };
    }

    public NsdManager getManager(){
        return nsdManager;
    }
}
